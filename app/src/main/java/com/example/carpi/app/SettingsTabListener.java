package com.example.carpi.app;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

/**
 * Created by juha on 6/14/14.
 */
public class SettingsTabListener implements ActionBar.TabListener {

    private Fragment fragment;

    public SettingsTabListener(Fragment fragment, Bundle fragmentArgs) {
        this.fragment = fragment;
        this.fragment.setArguments(fragmentArgs);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.replace(R.id.flSettingsContainer, fragment);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        fragmentTransaction.remove(fragment);
    }
}
