package com.example.carpi.app;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class SettingsActivity extends ActionBarActivity
    implements ModeFragment.OnModeListener,
        ModeFragment.OnCustomModeThrottleListener
{

    private static final String TAG = "SettingsActivity";

    private SharedPreferences sharedPreferences;
    private static final String KEY_MODE = "mode";
    private static final String KEY_CUSTOM_FORWARD_THROTTLE_LEVEL = "custom_mode_forward_throttle_level";
    private static final String KEY_CUSTOM_REVERSE_THROTTLE_LEVEL = "custom_mode_reverse_throttle_level";
    // Default values for driving mode
    private static final int defaultSelectedMode = R.id.rbModeTraining;
    private static int selectedMode;
    private static final int defaultCustomModeForwardThrottleLevel = 50;
    private static int customModeForwardThrottleLevel;
    private static final int defaultCustomModeReverseThrottleLevel = 50;
    private static int customModeReverseThrottleLevel;

    ///// Tabs and tab fragments

    // Mode
    private ActionBar.Tab tabMode;
    private Fragment modeFragment = new ModeFragment();

    // Network (car connection)
    private ActionBar.Tab tabNetwork;
    private Fragment networkFragment = new NetworkFragment();

    ///// Buttons

    // Open driving activity
    private Button bDrive;
    private Button bSavePreferences;

    /*
     * Activity lifecycle event handlers
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getPreferences(Context.MODE_PRIVATE);

        // Check if this is the first run
        if (!sharedPreferences.contains(KEY_MODE)) {
            // Assume that this is the first run if key KEY_MODE does not exist
            // Set default values and create preferences file
            selectedMode = defaultSelectedMode;
            customModeForwardThrottleLevel = defaultCustomModeForwardThrottleLevel;
            customModeReverseThrottleLevel = defaultCustomModeReverseThrottleLevel;
            savePreferences();
        } else {
            // Read settings from preferences file
            selectedMode = sharedPreferences.getInt(KEY_MODE, defaultSelectedMode);
            customModeForwardThrottleLevel = sharedPreferences.getInt(KEY_CUSTOM_FORWARD_THROTTLE_LEVEL,
                    defaultCustomModeForwardThrottleLevel);
            customModeReverseThrottleLevel = sharedPreferences.getInt(KEY_CUSTOM_REVERSE_THROTTLE_LEVEL,
                    defaultCustomModeReverseThrottleLevel);
        }

        Bundle fragmentArgs = new Bundle();
        fragmentArgs.putInt(KEY_MODE, selectedMode);
        fragmentArgs.putInt(KEY_CUSTOM_FORWARD_THROTTLE_LEVEL, customModeForwardThrottleLevel);
        fragmentArgs.putInt(KEY_CUSTOM_REVERSE_THROTTLE_LEVEL, customModeReverseThrottleLevel);

        // Add ActionBar for navigating between Settings views (mode, network, controller setup...).
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Tab Network (car connection)
        tabNetwork = actionBar.newTab().setText("Network");
        tabNetwork.setTabListener(new SettingsTabListener(networkFragment, fragmentArgs));
        actionBar.addTab(tabNetwork);

        // Tab Mode
        tabMode = actionBar.newTab().setText("Mode");
        tabMode.setTabListener(new SettingsTabListener(modeFragment, fragmentArgs));
        actionBar.addTab(tabMode);

        setContentView(R.layout.activity_settings);

        // Driving button
        bDrive = (Button)findViewById(R.id.bDrive);
        addDrivingListener();

        bSavePreferences = (Button)findViewById(R.id.bSaveSettings);
        addSavePreferencesListener();
        bSavePreferences.setEnabled(false); // Button disabled until settings have been changed
    }

    /*
     * Activity methods
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
     * Interfaces
     */

    @Override
    public void onModeChanged(int mode) {

        this.selectedMode = mode;
        bSavePreferences.setEnabled(hasPreferencesChanged());
    }

    @Override
    public void onCustomModeForwardThrottle(int throttleLevel) {

        this.customModeForwardThrottleLevel = throttleLevel;
        bSavePreferences.setEnabled(hasPreferencesChanged());
    }

    @Override
    public void onCustomModeReverseThrottle(int throttleLevel) {

        this.customModeReverseThrottleLevel = throttleLevel;
        bSavePreferences.setEnabled(hasPreferencesChanged());
    }

    /*
     * Helper functions
     */

    public void addDrivingListener() {
        bDrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent drivingIntent = new Intent(getApplicationContext(), DrivingActivity.class);
                startActivity(drivingIntent);

                String drivingMode;

                switch (selectedMode) {
                    case R.id.rbModeTraining:
                        drivingMode = "training";
                        break;
                    case R.id.rbModeRace:
                        drivingMode = "race";
                        break;
                    case R.id.rbModeCustom:
                        drivingMode = "custom";
                        break;
                    default:
                        Log.e(TAG, "unknown mode");
                        // ERROR: unknown mode; defaults to training mode
                        drivingMode = "training";
                        break;
                }

                if (drivingMode == "custom") {
                    drivingMode = String.format(CarPiProtocol.MODE_CUSTOM.toString(),
                            drivingMode,
                            customModeReverseThrottleLevel,
                            customModeForwardThrottleLevel);
                } else {
                    drivingMode = String.format(CarPiProtocol.MODE.toString(), drivingMode);
                }

                // Set driving mode
                SocketConnection.INSTANCE.send(drivingMode);

                // After receiving engine on signal the car changes to CONTROL DATA state and
                // accepts only steering and throttle commands.
                SocketConnection.INSTANCE.send(CarPiProtocol.ENGINE_ON.toString());

                // Start sending heartbeat signals
                SocketConnection.INSTANCE.startHeartbeat();
            }
        });
    }

    public void addSavePreferencesListener() {
        bSavePreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savePreferences();
                bSavePreferences.setEnabled(false);
            }
        });
    }

    private void savePreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_MODE, selectedMode);
        editor.putInt(KEY_CUSTOM_FORWARD_THROTTLE_LEVEL, customModeForwardThrottleLevel);
        editor.putInt(KEY_CUSTOM_REVERSE_THROTTLE_LEVEL, customModeReverseThrottleLevel);
        editor.commit();
    }

    private boolean hasPreferencesChanged() {
        // Compares whether current preferences differ from saved preferences
        if (sharedPreferences.contains(KEY_MODE)) {
            int tmpMode = sharedPreferences.getInt(KEY_MODE, defaultSelectedMode);

            if (tmpMode != this.selectedMode) return true;
        }

        if (sharedPreferences.contains(KEY_CUSTOM_FORWARD_THROTTLE_LEVEL)) {
            int tmpForwardThrottle = sharedPreferences.getInt(KEY_CUSTOM_FORWARD_THROTTLE_LEVEL,
                    defaultCustomModeForwardThrottleLevel);

            if (tmpForwardThrottle != this.customModeForwardThrottleLevel) return true;
        }

        if (sharedPreferences.contains(KEY_CUSTOM_REVERSE_THROTTLE_LEVEL)) {
            int tmpReverseThrottle = sharedPreferences.getInt(KEY_CUSTOM_REVERSE_THROTTLE_LEVEL,
                    defaultCustomModeReverseThrottleLevel);

            if (tmpReverseThrottle != this.customModeReverseThrottleLevel) return true;
        }

        return false; // No changes
    }
}
