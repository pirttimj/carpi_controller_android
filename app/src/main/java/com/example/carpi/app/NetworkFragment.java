package com.example.carpi.app;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by juha on 6/14/14.
 */
public class NetworkFragment extends Fragment {

    private static final String TAG = "NetworkFragment";

    private static final int REQUEST_PICK_WIFI_NETWORK = 1;
    private static final String ip = "192.168.42.1"; // CarPi default IP
    private static final int port = 10042; // CarPi default port number

    private Button bFindNetwork;
    private TextView tvSSID;
    private TextView tvSignalLevel;

    private WifiManager wifiManager;
    private WifiConnectionReceiver wifiConnectionReceiver;
    private IntentFilter wifiConnectionFilter;

    /*
     * Fragment lifecycle event handlers
     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        wifiManager = (WifiManager)getActivity().getSystemService(Context.WIFI_SERVICE);

        // Wifi Connection Receiver
        wifiConnectionFilter = new IntentFilter();
        wifiConnectionFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        wifiConnectionFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);
        wifiConnectionReceiver = new WifiConnectionReceiver();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_network, container, false);

        bFindNetwork = (Button)view.findViewById(R.id.bFindNetwork);
        addFindNetworkListener();

        // Fill network info views
        tvSSID = (TextView)view.findViewById(R.id.tvSSID);
        tvSignalLevel = (TextView)view.findViewById(R.id.tvSignalLevel);

        // Register wifi connection change listener
        getActivity().registerReceiver(this.wifiConnectionReceiver, wifiConnectionFilter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        wifiManager = (WifiManager)getActivity().getSystemService(Context.WIFI_SERVICE);

        // Register wifi connection change listener
        getActivity().registerReceiver(this.wifiConnectionReceiver, wifiConnectionFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Unregister wifi connection receiver
        getActivity().unregisterReceiver(this.wifiConnectionReceiver);
    }

    /*
     * Fragment methods
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PICK_WIFI_NETWORK) {
            // ACTION_PICK_WIFI_NETWORK does not (?) return a resultCode so ignore it

            String macAddress = wifiManager.getConnectionInfo().getMacAddress();

            // Initialize connection to car
            SocketConnection.INSTANCE.initConnection(ip, port, macAddress);

            // Send connect request to CarPi
            SocketConnection.INSTANCE.send(CarPiProtocol.CONNECTION_REQUEST.toString());
            // TODO: check that request is responded with {"connect":"ok"} message
            //       Continue if response ok, disconnect otherwise
        }
    }

    /*
     * Helper functions
     */

    private void addFindNetworkListener() {
        bFindNetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK),
                        REQUEST_PICK_WIFI_NETWORK);
            }
        });
    }

    /*
     * Private classes
     */

    private class WifiConnectionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            // Update SSID
            tvSSID.setText(wifiInfo.getSSID().replace("\"", ""));
            // Update signal level
            tvSignalLevel.setText(String.valueOf(wifiInfo.getRssi()));
        }
    }
}
