package com.example.carpi.app;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by juha on 6/17/14.
 */
public enum SocketConnection {
    // Recommended style for singleton
    INSTANCE;

    private static final String TAG = "SocketConnection";

    private Socket socket;
    private Thread thread;
    private String ip;
    private int port;
    private String id;
    private PrintWriter out;
    private CarPiHeartbeatTask carPiHeartbeat;
    private Timer heartBeatTimer;

    public void initConnection(String ip, int port, String id) {
        this.ip = ip;
        this.port = port;
        this.id = id;

        thread = new Thread(new SocketThread());
        thread.start();

        // Small delay to open socket connection
        try {
            Thread.sleep(500, 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void send(String message) {
        // Add id to message
        String msg = String.format(message, id);
        Log.i(TAG, "sending: " + msg);
        out.print(msg);
        out.flush();
    }

    private class SocketThread implements Runnable {

        @Override
        public void run() {
            try {
                socket = new Socket(ip, port);
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void startHeartbeat() {
        heartBeatTimer = new Timer();
        carPiHeartbeat = new CarPiHeartbeatTask();
        heartBeatTimer.schedule(carPiHeartbeat, 0, 100);
    }

    public void stopHeartbeat() {
        carPiHeartbeat.cancel();
    }

    private class CarPiHeartbeatTask extends TimerTask {

        @Override
        public void run() {
            send(CarPiProtocol.HEARTBEAT.toString());
        }
    }
}
