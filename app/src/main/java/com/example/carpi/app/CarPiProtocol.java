package com.example.carpi.app;

/**
 * Created by juha on 6/18/14.
 */
public enum CarPiProtocol {

    /*
     * STATE 0 (CONNECTION STATE)
     */

    CONNECTION_REQUEST {
        @Override
        public String toString() {
                return "{\"connect\":\"%s\"}";
            }
    },

    /*
     * STATE 1 (INITIALIZATION STATE)
     */

    GET {
        @Override
        public String toString() {
            return "{\"get\":\"%s\", \"id\":\"%%s\"}";
        }
    },

    INIT {
        @Override
        public String toString() {
            return "{\"init\":\"%s\", \"id\":\"%%s\"}";
        }
    },

    MODE {
        @Override
        public String toString() {
            return "{\"mode\":\"%s\", \"id\":\"%%s\"}";
        }
    },

    MODE_CUSTOM {
        @Override
        public String toString() {
            return "{\"mode\":\"%s\", \"min_throttle\":%d, \"max_throttle\":%d, \"id\":\"%%s\"}";
        }
    },

    DISCONNECT {
        @Override
        public String toString() {
            return "{\"disconnect\":\"%s\"}";
        }
    },

    ENGINE_ON {
        @Override
        public String toString() {
            return "{\"engine\":\"on\", \"id\":\"%s\"}";
        }
    },

    /*
     * STATE 2 (CONTROL DATA)
     */

    HEARTBEAT {
        @Override
        public String toString() {
            return "{\"heartbeat\":true, \"id\":\"%s\"}";
        }
    },

    STEERING {
        @Override
        public String toString() {
            return "{\"steering\":%d, \"id\":\"%%s\"}";
        }
    },

    THROTTLE {
        @Override
        public String toString() {
            return "{\"throttle\":%d, \"id\":\"%%s\"}";
        }
    },

    ENGINE_OFF {
        @Override
        public String toString() {
            return "{\"engine\":\"off\", \"id\":\"%s\"}";
        }
    };
}
