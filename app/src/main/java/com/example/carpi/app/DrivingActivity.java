package com.example.carpi.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.SeekBar;

import com.example.carpi.app.R;

import java.net.Socket;

public class DrivingActivity extends ActionBarActivity {

    private static final String TAG = "DrivingActivity";

    private static final int THROTTLE = 1000;
    private static final int STEERING = 1000;

    SeekBar sbThrottle;
    SeekBar sbSteering;

    /*
     * Activity lifecycle event handlers
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Maximize the screen area for Driving Activity
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_driving);

        sbThrottle = (SeekBar)findViewById(R.id.sbThrottle);
        sbSteering = (SeekBar)findViewById(R.id.sbSteering);

        sbThrottle.setProgress(THROTTLE/2);
        sbSteering.setProgress(STEERING/2);

        addThrottleListener();
        addSteeringListener();
    }

    /*
     * Helper functions
     */

    private void addThrottleListener() {
        sbThrottle.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.i(TAG, "Throttle: " + seekBar.getProgress());
                SocketConnection.INSTANCE.send(String.format(CarPiProtocol.THROTTLE.toString(),
                        seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Return to neutral position
                seekBar.setProgress(THROTTLE/2);
            }
        });
    }

    private void addSteeringListener() {
        sbSteering.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                Log.i(TAG, "Steering: " + seekBar.getProgress());
                SocketConnection.INSTANCE.send(String.format(CarPiProtocol.STEERING.toString(),
                        seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setProgress(STEERING/2);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        SocketConnection.INSTANCE.stopHeartbeat();
        // Tell car to return back to INITIALIZATION STATE
        SocketConnection.INSTANCE.send(CarPiProtocol.ENGINE_OFF.toString());
    }
}
