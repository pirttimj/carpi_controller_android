package com.example.carpi.app;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by juha on 6/13/14.
 */
public class ModeFragment extends Fragment {

    private static final String TAG = "ModeFragment";

    OnModeListener callBackMode;
    OnCustomModeThrottleListener callBackCustomModeForwardThrottle;
    OnCustomModeThrottleListener callBackCustomModeReverseThrottle;

    // Container activity must implement these interfaces
    public interface OnModeListener {
        public void onModeChanged(int mode);
    }

    public interface OnCustomModeThrottleListener {
        public void onCustomModeForwardThrottle(int throttleLevel);
        public void onCustomModeReverseThrottle(int throttleLevel);
    }

    private RadioGroup rgModeSelection;
    private LinearLayout llCustomControls;
    private SeekBar sbCustomModeForwardThrottleLevel;
    private SeekBar sbCustomModeReverseThrottleLevel;
    private TextView tvCustomModeForwardThrottleLevel;
    private TextView tvCustomModeReverseThrottleLevel;

    /*
     * Fragment lifecycle event handlers
     */

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Check that containing activity implements required callback interface
        try {
            callBackMode = (OnModeListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnModeListener");
        }

        try {
            callBackCustomModeForwardThrottle = (OnCustomModeThrottleListener)activity;
            callBackCustomModeReverseThrottle = (OnCustomModeThrottleListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException((activity.toString() + " must implement OnCustomModeThrottleListener"));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mode, container, false);

        llCustomControls = (LinearLayout)view.findViewById(R.id.llCustomControls);

        rgModeSelection = (RadioGroup)view.findViewById(R.id.rgModeSelection);
        addModeListener();
        rgModeSelection.check(getArguments().getInt("mode"));

        sbCustomModeForwardThrottleLevel = (SeekBar)view.findViewById(R.id.sbCustomModeForwardThrottleLevel);
        tvCustomModeForwardThrottleLevel = (TextView)view.findViewById(R.id.tvCustomModeForwardThrottleLevel);
        addCustomModeForwardThrottleListener();
        sbCustomModeForwardThrottleLevel.setProgress(getArguments().getInt("custom_mode_forward_throttle_level"));

        sbCustomModeReverseThrottleLevel = (SeekBar)view.findViewById(R.id.sbCustomModeReverseThrottleLevel);
        tvCustomModeReverseThrottleLevel = (TextView)view.findViewById(R.id.tvCustomModeReverseThrottleLevel);
        addCustomModeReverseThrottleListener();
        sbCustomModeReverseThrottleLevel.setProgress(getArguments().getInt("custom_mode_reverse_throttle_level"));

        return view;
    }

    /*
     * Helper functions
     */

    private void addModeListener() {
        rgModeSelection.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                // i is the checked radio button

                if (i == R.id.rbModeCustom) {
                    llCustomControls.setVisibility(View.VISIBLE);
                } else {
                    llCustomControls.setVisibility(View.INVISIBLE);
                }

                callBackMode.onModeChanged(i); // Sent the selected mode number to hosting activity
            }
        });
    }

    private void addCustomModeForwardThrottleListener() {
        sbCustomModeForwardThrottleLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tvCustomModeForwardThrottleLevel.setText(
                        String.valueOf(sbCustomModeForwardThrottleLevel.getProgress()) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                callBackCustomModeForwardThrottle.onCustomModeForwardThrottle(
                        sbCustomModeForwardThrottleLevel.getProgress());
            }
        });
    }

    private void addCustomModeReverseThrottleListener() {
        sbCustomModeReverseThrottleLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                tvCustomModeReverseThrottleLevel.setText(
                        String.valueOf(sbCustomModeReverseThrottleLevel.getProgress()) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                callBackCustomModeReverseThrottle.onCustomModeReverseThrottle(
                        sbCustomModeReverseThrottleLevel.getProgress());
            }
        });
    }
}
