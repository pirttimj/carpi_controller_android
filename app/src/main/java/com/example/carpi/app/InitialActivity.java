package com.example.carpi.app;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.example.carpi.app.R;

public class InitialActivity extends ActionBarActivity {

    private static final String TAG = "InitialActivity";

    private static final int REQUEST_PICK_WIFI_NETWORK = 1;

    /*
     * Activity lifecycle event handlers
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_initial);

        Intent intent = new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK);
        startActivityForResult(intent, REQUEST_PICK_WIFI_NETWORK);
    }

    /*
     * Activity methods
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PICK_WIFI_NETWORK) {
            // ACTION_PICK_WIFI_NETWORK does not (?) return a resultCode so ignore it

            // Start Settings activity
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.initial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
